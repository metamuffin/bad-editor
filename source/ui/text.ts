import { State, UIComponent } from "./component";


export class UIText extends UIComponent {
    content: string
    style?: string

    constructor(content: string) {
        super();
        this.content = content
    }

    draw(): State {
        return {
            chars: this.content.split("").map((c,i) => ({
                content: c,
                x: i,
                y: 0,
                style: this.style
            })),
            height: 1,
            width: this.content.length
        }
    }
}