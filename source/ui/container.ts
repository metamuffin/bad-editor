import { CharUpdate, State, UIComponent } from "./component"
import { u_translate } from "./helper"
import { UISingleChildComponent } from "./singlechild"

export interface Edges {
    top: number
    left: number
    bottom: number
    right: number
}
export interface BorderChars {
    vertical: string
    horizontal: string
    top_left: string
    top_right: string
    bottom_left: string
    bottom_right: string
}

export class UIContainer extends UISingleChildComponent {
    border?: string
    margin: Edges = { bottom: 0, left: 0, right: 0, top: 0 }
    padding: Edges = { top: 0, right: 0, left: 0, bottom: 0 }
    border_chars: BorderChars = { horizontal: "─", vertical: "│", top_left: "┌", top_right: "┐", bottom_left: "└", bottom_right: "┘" }

    constructor() {
        super()
    }

    set_child(child: UIComponent) {
        super.set_child(child)
        let border_size = this.border ? 1 : 0
        if (this.max_width) child.max_width = this.max_width - this.margin.left - border_size - this.padding.left - this.padding.right - border_size - this.margin.right
        if (this.max_height) child.max_height = this.max_height - this.margin.top - border_size - this.padding.top - this.padding.bottom - border_size - this.margin.bottom
    }

    draw(): State {
        if (!this.child) return { height: 0, width: 0, chars: [] }
        let border_size = this.border ? 1 : 0
        let cr = this.child.get_content()

        return {
            width: this.margin.left + border_size + this.padding.left + cr.width + this.padding.right + border_size + this.margin.right,
            height: this.margin.top + border_size + this.padding.top + cr.height + this.padding.bottom + border_size + this.margin.bottom,
            chars: [
                ...u_translate(cr.chars, this.margin.left + border_size + this.padding.left, this.margin.top + border_size + this.padding.top),
                ...u_translate(
                    this.draw_border(this.padding.left + cr.width + this.padding.right, this.padding.top + cr.height + this.padding.bottom),
                    this.margin.left,
                    this.margin.top
                )
            ]
        }
    }

    draw_border(width: number, height: number): CharUpdate[] {
        if (!this.border) return []
        let r: CharUpdate[] = [
            { x: 0, y: 0, content: this.border_chars.top_left, style: this.border },
            { x: width + 1, y: 0, content: this.border_chars.top_right, style: this.border },
            { x: 0, y: height + 1, content: this.border_chars.bottom_left, style: this.border },
            { x: width + 1, y: height + 1, content: this.border_chars.bottom_right, style: this.border },

        ]
        for (let x = 0; x < width; x++) {
            r.push({ x: x + 1, y: 0, content: this.border_chars.horizontal, style: this.border })
            r.push({ x: x + 1, y: height + 1, content: this.border_chars.horizontal, style: this.border })
        }
        for (let y = 0; y < height; y++) {
            r.push({ x: 0, y: y + 1, content: this.border_chars.vertical, style: this.border })
            r.push({ x: width + 1, y: y + 1, content: this.border_chars.vertical, style: this.border })
        }
        return r
    }

}