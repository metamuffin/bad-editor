import { State, UIComponent } from "./component";
import { UIInputEvent } from "./input";

export class UISingleChildComponent extends UIComponent {
    child?: UIComponent

    constructor(child?: UIComponent) {
        super()
        if (child) this.set_child(child)
    }

    on_input(ev: UIInputEvent): boolean {
        if (!this.child) return false
        return this.child.on_input(ev)
    }

    set_child(child: UIComponent) {
        this.child = child
        child.max_width = this.max_width
        child.max_height = this.max_height
        this.child.parent = this
        this.child.on_enter_tree()
        this.bubble_update()
    }
    clear_child() {
        this.child?.on_leave_tree()
        this.child = undefined
        this.bubble_update()
    }

    draw(): State {
        return this.child?.get_content() ?? { chars: [], width: 0, height: 0 }
    }
}

