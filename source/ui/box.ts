import { State, UIComponent } from "./component";
import { UISingleChildComponent } from "./singlechild";


export class UIBox extends UISingleChildComponent {
    width: number
    height: number
    constructor(w: number, h: number) {
        super()
        this.width = w
        this.height = h
    }

    set_child(child: UIComponent) {
        super.set_child(child)
        if (!this.child) return
        this.child.max_height = this.height
        this.child.max_width = this.width
    }

    draw(): State {
        if (!this.child) return { chars: [], width: 0, height: 0 }
        let cr = this.child.get_content()
        return {
            chars: cr.chars.filter(u => u.x < this.width && u.y < this.height),
            width: this.width,
            height: this.height,
        }
    }
}

