import { CharUpdate, State, UIComponent } from "./component";
import { UISingleChildComponent } from "./singlechild";

export class UIViewport extends UISingleChildComponent {
    last_frame: Map<string, CharUpdate>

    constructor(child: UIComponent) {
        super()
        this.last_frame = new Map()
        this.set_child(child)
    }

    bubble_update() {

        let r = this.get_content()
        let k = r.chars
        
        // process.stdout.write("\x1b[2J")
        let this_frame = new Map<string, CharUpdate>()
        k.forEach(u => this_frame.set(`${u.x}_${u.y}`, u))

        let need_draw = []
        for (const tu of this_frame.values()) {
            const lu = this.last_frame.get(`${tu.x}_${tu.y}`)
            if (lu != tu) need_draw.push(tu)
        }

        let o = ""
        for (const u of need_draw) {
            o += `\x1b[${u.y + 1};${u.x + 1}H${u.style ?? ""}${u.content}\x1b[0m`
        }
        process.stdout.write(o)
        // console.log(JSON.stringify(o));
        // console.log(k);
        // process.stdout.write("\x1b[20;H")
        

        this.last_frame = this_frame
    }

}

