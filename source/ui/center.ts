import { State } from "./component";
import { u_translate } from "./helper";
import { UISingleChildComponent } from "./singlechild";

export class UICenter extends UISingleChildComponent {
    draw(): State {
        if (!this.child) return { chars: [], width: 0, height: 0 }
        if (!this.max_height || !this.max_width) return super.draw()
        let cr = this.child.get_content()

        let cx = Math.floor(this.max_width / 2)
        let cy = Math.floor(this.max_height / 2)
        let rx = Math.floor(cr.width / 2)
        let ry = Math.floor(cr.height / 2)

        return {
            chars: u_translate(cr.chars, cx - rx, cy - ry),
            height: this.max_height,
            width: this.max_width,
        }
    }
}
