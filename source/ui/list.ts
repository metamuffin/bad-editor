import { State, UIComponent } from "./component";
import { u_translate } from "./helper";

export abstract class UIList extends UIComponent {
    children: UIComponent[] = []

    append(...children: UIComponent[]) {
        children.forEach(c => {
            c.parent = this
            c.on_enter_tree()
            this.children.push(c)
        })
        this.bubble_update()
    }
    remove(...children: UIComponent[]) {
        for (const c of children) {
            const i = this.children.findIndex(d => d == c)
            if (i == -1) throw new Error("ksadhfjskdhfjks");
            const [r] = this.children.splice(i, 1)
            r.on_leave_tree()
        }
    }
}


export class UIVerticalList extends UIList {
    draw(): State {
        let y_off = 0
        let s: State = { width: 0, height: 0, chars: [] }
        for (const c of this.children) {
            const k = c.get_content()
            s.height += k.height
            s.width = Math.max(s.width, k.width)
            s.chars.push(...u_translate(k.chars, 0, y_off))
            y_off += k.height
        }
        return s
    }

}

export class UIHorizontallList extends UIList {
    draw(): State {
        let x_off = 0
        let s: State = { width: 0, height: 0, chars: [] }
        for (const c of this.children) {
            const k = c.get_content()
            s.width += k.width
            s.height = Math.max(s.height, k.height)
            s.chars.push(...u_translate(k.chars, x_off, 0))
            x_off += k.width
        }
        return s
    }

}