

export interface UIInputEvent {
    key: string
    control: boolean
    shift: boolean
    alt: boolean
}

