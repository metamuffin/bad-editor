import { CharUpdate } from "./component";

export function u_translate(u: CharUpdate[], x: number, y: number): CharUpdate[] {
    return u.map(u => ({
        ...u,
        x: u.x + x,
        y: u.y + y,
    }))
}