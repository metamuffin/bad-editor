import { UIInputEvent } from "./input"

export interface CharUpdate {
    x: number,
    y: number,
    content: string
    style?: string
}
export interface State {
    chars: CharUpdate[]
    width: number
    height: number
}



export abstract class UIComponent {
    needs_update: boolean = false
    parent?: UIComponent
    cached_state?: State

    max_width?: number
    max_height?: number

    has_focus: boolean = false

    constructor() { }

    on_focus() { this.has_focus = true }
    on_blur() { this.has_focus = false }

    // returns wheter the event was handled
    on_input(ev: UIInputEvent): boolean { return false }

    on_enter_tree() { }
    on_leave_tree() { }

    bubble_update() {
        this.needs_update = true
        this.cached_state = undefined
        this.parent?.bubble_update()
    }

    get_content(): State {
        if (this.cached_state) return this.cached_state
        return this.cached_state = this.draw()
    }

    abstract draw(): State
}
