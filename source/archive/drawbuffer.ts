export interface DrawChar {
    content: string
    style: string
}

export class DrawBuffer {
    width: number
    height: number
    content: (DrawChar | undefined)[][] = []


    constructor(width: number, height: number) {
        this.width = width; this.height = height
        this.resize(width, height)
    }
    resize(width: number, height: number) {
        this.width = width
        this.height = height
        this.content = []
        for (let x = 0; x < width; x++) {
            let k = []
            for (let y = 0; y < height; y++) {
                k.push(undefined)
            }
            this.content.push(k)
        }
    }

    get(x: number, y: number) {
        if (this.content[x]) return undefined
        return this.content[x][y]
    }
    set(x: number, y: number, c: DrawChar) {
        if (!this.content[x]) return
        this.content[x][y] = c
    }
    add(x: number, y: number, c?: DrawChar) {
        if (!this.content[x]) return
        if (!c) return
        this.content[x][y] = c
    }

    add_from(source: DrawBuffer, dxoff: number, dyoff: number) {
        for (let x = 0; x < source.width; x++) {
            for (let y = 0; y < source.height; y++) {
                this.add(x + dxoff, y + dyoff, source.get(x, y))
            }
        }
    }

}